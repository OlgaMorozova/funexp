var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0, 'Николай Фролов', 0, 'Олег Боровой', 0];


var studentsNames = studentsAndPoints.filter(function(names) {
  return isNaN(names);
});
var studentsPoints = studentsAndPoints.filter(function(points) {
  return points % 2 === 0;
});


console.log(studentsNames);
console.log(studentsPoints);