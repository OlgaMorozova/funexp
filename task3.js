var studentsNames = ['Алексей Петров', 'Ирина Овчинникова', 'Глеб Стукалов', 'Антон Павлович', 'Виктория Заровская', 'Алексей Левенец', 'Тимур Вамуш', 'Евгений Прочан', 'Александр Малов', 'Николай Фролов', 'Олег Боровой'];

var studentsPoints = [0, 60, 30, 30, 30, 70, 30, 60, 0, 0, 0];

var max, maxIndex;
studentsPoints.forEach(function (number, i) {
if (!max || number > max) {
max = number;
maxIndex = i;
}
});
console.log("Студент набравший максимальный балл:", studentsNames[maxIndex], "\(", max, "баллов\)");